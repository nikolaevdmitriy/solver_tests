package com.solver.services;

public class TestConfig {

    public static final String baseApiKey = "solver_app";
    public static final String privateApiUrl = "https://test.a24.biz/app/api/auth";
    public static final String publicApiUrl = "https://test.a24.biz/app/api/public";
    public static final String publicOpenGQLUrl = "https://test.a24.biz/gql/api/public";
    public static final String publicFileApiUrl = "https://test.a24.biz/fileapi/upload";
    public static final String publicGetFileUrl = "https://test.a24.biz/file";

    public static final String baseApiToken = "19f7709876de4345f56e67152eb70a97";
    public static final String performerApiToken = "2fdb3d2c28946dde0329cc8f86baa4ba";
}
