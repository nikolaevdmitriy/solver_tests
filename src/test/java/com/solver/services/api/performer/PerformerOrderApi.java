package com.solver.services.api.performer;

import com.solver.scheme.performer.Mutation;
import com.solver.services.Config;
import com.solver.services.entity.OfferEntity;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;

import static com.solver.services.api.Sender.sendPost;

public class PerformerOrderApi {

    public JsonPath takeOrder(OfferEntity offer) {

        String scheme = Mutation.takeOrder(offer);
        RequestSpecification spec = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(Config.publicOpenGQLUrl)
                .addHeader("Token", Config.performerApiToken)
//                .addFilter(new ResponseLoggingFilter())
                .build();

        return sendPost(spec, scheme);
    }
}
