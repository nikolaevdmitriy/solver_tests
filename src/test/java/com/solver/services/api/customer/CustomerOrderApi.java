package com.solver.services.api.customer;

import com.solver.scheme.customer.Mutation;
import com.solver.scheme.customer.Query;
import com.solver.services.Config;
import com.solver.services.entity.OrderEntity;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;

import static com.solver.services.api.Sender.sendPost;

public class CustomerOrderApi {

    public JsonPath addComment(Integer offerId, String text) {

        String scheme = Mutation.addComment(offerId, text);
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath create(OrderEntity order) {

        String scheme = Mutation.createOrder(order);
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath update(Integer orderId, OrderEntity order) {

        String scheme = Mutation.updateOrder(orderId, order);
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath delete(Integer orderId) {

        String scheme = Mutation.deleteOrder(orderId);
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath addFileToOrder(Integer orderId, Integer fileId) {

        String scheme = Mutation.addFileToOrder(orderId, fileId);
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath price(OrderEntity order) {

        String scheme = Query.price(order);
        RequestSpecification spec = Config.getSpecWithoutToken(Config.publicApiUrl);

        return sendPost(spec, scheme);
    }

    public JsonPath increaseDeadline(Integer orderId) {

        String scheme = Mutation.increaseDeadline(orderId);
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath dictionaryList() {

        String scheme = Query.dictionaryList();
        RequestSpecification spec = Config.getSpecWithoutToken(Config.publicApiUrl);

        return sendPost(spec, scheme);
    }

    public JsonPath faq() {

        String scheme = Query.faq();
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath paymentMethods(Integer amount, Integer countryId) {

        String scheme = Query.paymentMethods(amount, countryId);
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath paymentCountry() {

        String scheme = Query.paymentCountry();
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath paymentUrl(Integer amount, Integer paymentMethodId, Integer offerId) {

        String scheme = Query.paymentUrl(amount, paymentMethodId, offerId);
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }
}
