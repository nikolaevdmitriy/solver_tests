package com.solver.services.api.customer;

import com.solver.scheme.customer.Mutation;
import com.solver.services.Config;
import com.solver.services.entity.UserEntity;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;

import static com.solver.services.api.Sender.sendPost;

public class CustomerAuthApi {

    public JsonPath getToken(String apiKey) {

        String scheme = Mutation.login(apiKey);
        RequestSpecification spec = Config.getSpecWithoutToken();

        return sendPost(spec, scheme);
    }

    public JsonPath refreshToken(String currentToken) {

        String scheme = Mutation.refreshToken();
        RequestSpecification spec = Config.getSpecWithToken(Config.privateApiUrl, currentToken);

        return sendPost(spec, scheme);
    }

    public JsonPath login(UserEntity user, String apiKey) {

        String scheme = Mutation.login(user, apiKey);
        RequestSpecification spec = Config.getSpecWithoutToken();

        return sendPost(spec, scheme);
    }

    public JsonPath changeUser(UserEntity user, String apiKey, String token) {

        String scheme = Mutation.changeUser(user, apiKey, token);
        RequestSpecification spec = Config.getSpecWithoutToken();

        return sendPost(spec, scheme);
    }

    public JsonPath registration(String email, String apiKey) {

        String scheme = Mutation.registration(email, apiKey);
        RequestSpecification spec = Config.getSpecWithoutToken();

        return sendPost(spec, scheme);
    }

    public JsonPath registration(String email, String apiKey, String currentToken) {

        String scheme = Mutation.registration(email, apiKey, currentToken);
        RequestSpecification spec = Config.getSpecWithoutToken();

        return sendPost(spec, scheme);
    }
}
