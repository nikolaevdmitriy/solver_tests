package com.solver.services.api.customer;

import com.solver.scheme.customer.Mutation;
import com.solver.services.Config;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;

import java.io.File;

import static com.solver.services.api.Sender.getFile;
import static com.solver.services.api.Sender.sendPost;
import static com.solver.services.api.Sender.uploadFile;

public class CustomerFileApi {

    public JsonPath upload(File file) {

        RequestSpecification spec = Config.getSpecWithTokenForUpload(Config.publicFileApiUrl);

        return uploadFile(spec, file);
    }

    public JsonPath delete(String fileHash) {

        String scheme = Mutation.deleteFile(fileHash);
        RequestSpecification spec = Config.getSpecWithToken();

        return sendPost(spec, scheme);
    }

    public JsonPath get(String fileHash) {

        RequestSpecification spec = Config.getSpecWithToken(Config.publicGetFileUrl + "/" + fileHash);

        return getFile(spec);
    }
}
