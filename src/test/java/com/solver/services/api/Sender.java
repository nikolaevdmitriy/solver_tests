package com.solver.services.api;

import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import java.io.File;

import static io.restassured.RestAssured.given;

public class Sender {

    public static JsonPath sendPost(RequestSpecification spec, String scheme) {

        JSONObject requestBody = new JSONObject();
        requestBody.put("query", scheme);

        return given()
                .spec(spec)
                .body(requestBody.toString())
                .when()
                .post()
                .then()
                .statusCode(200)
                .extract()
                .jsonPath();
    }

    public static JsonPath uploadFile(RequestSpecification spec, File file) {

        return given()
                .spec(spec)
                .multiPart(file)
                .when()
                .post()
                .then()
                .statusCode(200)
                .extract()
                .jsonPath();
    }

    public static JsonPath getFile(RequestSpecification spec) {

        return given()
                .spec(spec)
                .when()
                .post()
                .then()
                .statusCode(200)
                .extract()
                .jsonPath();
    }
}
