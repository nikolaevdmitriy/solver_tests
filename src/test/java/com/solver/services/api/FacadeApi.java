package com.solver.services.api;

import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.api.performer.PerformerOrderApi;
import com.solver.services.entity.OfferEntity;
import com.solver.services.entity.OrderEntity;

public class FacadeApi {

    public static OfferEntity createOffer() {

        CustomerOrderApi customerApi = new CustomerOrderApi();
        PerformerOrderApi performerApi = new PerformerOrderApi();

        OrderEntity order = new OrderEntity();
        Integer orderId = customerApi.create(order.fillFakeData()).getInt("data.createOrder.id");

        OfferEntity offer = new OfferEntity();
        offer.setOrderId(orderId);
        offer.fillFakeMessage();

        Integer offerId = performerApi.takeOrder(offer).getInt("data.orderCreateOffer.id");
        offer.setId(offerId);

        return offer;
    }
}
