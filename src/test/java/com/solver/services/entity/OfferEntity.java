package com.solver.services.entity;

import com.github.javafaker.Faker;

public class OfferEntity {
    private String message;
    private Integer orderId;
    private Integer id;
    private Integer bid;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return this.message;
    }

    public Integer getBid() { return this.bid;}

    public Integer getOrderId() {
        return this.orderId;
    }

    public Integer getId() {
        return this.id;
    }

    public void setBid(Integer bid) {this.bid = bid;}

    public void fillFakeMessage() {

        Faker faker = new Faker();
        Integer bid = faker.number().numberBetween(100, 1000);

        this.setBid(bid);
    }
}
