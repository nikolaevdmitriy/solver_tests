package com.solver.services.entity;

import com.github.javafaker.Faker;

public class OrderEntity {
    private String description;
    private Integer deadlineMinutes;
    private Integer categoryId;
    private Integer countTasks;

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDeadlineMinutes(Integer deadlineMinutes) {
        this.deadlineMinutes = deadlineMinutes;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public void setCountTasks(Integer countTasks) {
        this.countTasks = countTasks;
    }

    public String getDescription() {
        return this.description;
    }

    public Integer getDeadlineMinutes() {
        return this.deadlineMinutes;
    }

    public Integer getCategoryId() {
        return this.categoryId;
    }

    public Integer getCountTasks() {
        return this.countTasks;
    }

    public OrderEntity fillFakeData() {

        Faker faker = new Faker();
        String description = String.format("Автотест %s", faker.lorem().word());
        Integer deadlineMinutes = 30;

        Integer categoryId = faker.number().numberBetween(1, 10);
        Integer countTasks = faker.number().numberBetween(1, 20);

        this.setDescription(description);
        this.setDeadlineMinutes(deadlineMinutes);
        this.setCategoryId(categoryId);
        this.setCountTasks(countTasks);

        return  this;
    }

}
