package com.solver.services;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class Config {

    public static final String baseApiKey = "solver_app";
    public static final String privateApiUrl = "https://a24.biz/app/api/auth";
    public static final String publicApiUrl = "https://a24.biz/app/api/public";
    public static final String publicOpenGQLUrl = "https://a24.biz/gql/api/public";
    public static final String publicFileApiUrl = "https://a24.biz/fileapi/upload";
    public static final String publicGetFileUrl = "https://a24.biz/file";

    public static final String baseApiToken = "7e6cb24776c7a3d801300d45a14dc4a3";
    public static final String performerApiToken = "856c70df3e549b56202a0032ed7d6358";

    public static RequestSpecification getSpecWithToken()
    {
        return new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(Config.publicApiUrl)
                .addHeader("Token", Config.baseApiToken)
//                .addFilter(new ResponseLoggingFilter())
                .build();
    }

    public static RequestSpecification getSpecWithToken(String url)
    {
        return new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(url)
                .addHeader("Token", Config.baseApiToken)
//                .addFilter(new ResponseLoggingFilter())
                .build();
    }

    public static RequestSpecification getSpecWithToken(String url, String token)
    {
        return new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(url)
                .addHeader("Token", token)
//                .addFilter(new ResponseLoggingFilter())
                .build();
    }

    public static RequestSpecification getSpecWithTokenForUpload(String url)
    {
        return new RequestSpecBuilder()
                .setContentType("multipart/form-data")
                .setBaseUri(url)
                .addHeader("Token", Config.baseApiToken)
//                .addFilter(new ResponseLoggingFilter())
                .build();
    }

    public static RequestSpecification getSpecWithoutToken()
    {
        return new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(Config.privateApiUrl)
//                .addFilter(new ResponseLoggingFilter())
                .build();
    }

    public static RequestSpecification getSpecWithoutToken(String url)
    {
        return new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(url)
//              .addFilter(new ResponseLoggingFilter())
                .build();
    }
}
