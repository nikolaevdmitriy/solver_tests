package com.solver.scheme.performer;

import com.solver.services.entity.OfferEntity;

public final class Mutation {

    public static String takeOrder(OfferEntity offer)
    {
        return String.format("mutation{ orderCreateOffer(id: \"%d\",bid: %d) { id origin_bid bid text expired } }",
                offer.getOrderId(),
                offer.getBid()
        );
    }
}
