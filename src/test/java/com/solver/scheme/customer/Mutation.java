package com.solver.scheme.customer;

import com.solver.services.entity.OrderEntity;
import com.solver.services.entity.UserEntity;

public final class Mutation {

    public static String addComment(Integer offerId, String text)
    {
        return String.format(
                "mutation {addComment(offerId: %d, text: \"%s\") {text}}",
                offerId,
                text
        );
    }

    public static String createOrder(OrderEntity order)
    {
        return String.format(
                "mutation {createOrder(order: {description: \"%s\", deadlineMinutes: %d, categoryId: %d}) { id description isSolver budget}}",
                order.getDescription(),
                order.getDeadlineMinutes(),
                order.getCategoryId()
        );
    }

    public static String updateOrder(Integer orderId, OrderEntity order)
    {
        return String.format(
                "mutation { updateOrder(id: %d, order: { description: \"%s\",  deadlineMinutes: %d, categoryId: \"%d\", fileIds: [8232269] }) { id description categoryId budget}}",
                orderId,
                order.getDescription(),
                order.getDeadlineMinutes(),
                order.getCategoryId()
        );
    }

    public static String deleteOrder(Integer orderId)
    {
        return String.format("mutation {deleteOrder(id: %d)}", orderId);
    }

    public static String increaseDeadline(Integer orderId)
    {
        return String.format("mutation {increaseDeadline(orderId: %d){id}}", orderId);
    }

    public static String login(String apiKey)
    {
        return String.format("mutation {login(apiKey: \"%s\") {token}}",
                apiKey
        );
    }

    public static String login(UserEntity user, String apiKey)
    {
        return String.format("mutation {login(apiKey: \"%s\", login: \"%s\", password: \"%s\") {token}}",
                apiKey,
                user.getLogin(),
                user.getPassword()
        );
    }

    public static String addFirebaseToken(String token)
    {
        return String.format(
                "mutation {addFirebaseToken(firebaseToken: \"%s\", userId: \"%d\")}",
                token,
                178645
        );
    }

    public static String registration(String email, String apiKey) {

        return String.format(
                "mutation {registration(email: \"%s\", apiKey: \"%s\"){token}}",
                email,
                apiKey
        );
    }

    public static String registration(String email, String apiKey, String currentToken) {

        return String.format(
                "mutation {registration(email: \"%s\", apiKey: \"%s\", token: \"%s\"){token}}",
                email,
                apiKey,
                currentToken
        );
    }

    public static String checkUniqueEmail(String email) {

        return String.format (
                "mutation {checkUniqueEmail(email: \"%s\")}",
                email
        );
    }

    public static String changeUser(UserEntity user, String apiKey, String token) {

        return String.format (
                "mutation {changeUser(token: \"%s\", apiKey: \"%s\", login: \"%s\", password: \"%s\"){token}}",
                token,
                apiKey,
                user.getLogin(),
                user.getPassword()
        );
    }

    public static String refreshToken() {

        return "mutation {refreshToken}";
    }

    public static String addFileToOrder(Integer orderId, Integer fileId) {

        return String.format("mutation {addFileToOrder(orderId: %d, fileId: %d) {fileHash}}",
                orderId,
                fileId
        );
    }

    public static String deleteFile(String fileHash) {

        return String.format("mutation {deleteFile(fileHash: \"%s\")}",
                fileHash
        );
    }
}

