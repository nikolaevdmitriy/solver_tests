package com.solver.scheme.customer;

import com.solver.services.entity.OrderEntity;

public final class Query {

    public static String price(OrderEntity order) {

        return String.format("{price(categoryId: %d , countTasks: %d, deadlineMinutes: %d)}",
                order.getCategoryId(),
                order.getCountTasks(),
                order.getDeadlineMinutes()
        );
    }

    public static String dictionaryList() {

        return "{dictionarylist {maincategories {id name groupName groupId}, workcategories {id name groupName groupId}}}";
    }

    public static String faq() {

        return "{faq {question answer }}";
    }

    public static String paymentMethods(Integer amount, Integer countryId) {

        return String.format("{paymentMethods(amount: %d, countryId: %d) {id taxFormula calculatedTax name group identifier methods {id calculatedTax name}}}",
                amount,
                countryId
        );
    }

    public static String paymentCountry() {

        return "{dictionarylist {paymentcountry {id name }}}";
    }

    public static String paymentUrl(Integer amount, Integer paymentMethodId, Integer offerId) {

        return String.format("{paymentUrl(amount: %d, paymentMethodId: %d, offerId: %d)}",
                amount,
                paymentMethodId,
                offerId
        );
    }
}

