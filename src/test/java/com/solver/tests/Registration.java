package com.solver.tests;

import com.github.javafaker.Faker;
import com.solver.services.Config;
import com.solver.services.api.customer.CustomerAuthApi;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Registration {

    private static CustomerAuthApi api;
    private static String currentToken;

    @BeforeClass
    public static void initSpec(){
        api = new CustomerAuthApi();

        JsonPath response = api.getToken(Config.baseApiKey);
        currentToken = response.getString("data.login.token");
    }

    @DataProvider
    public static Object[][] dataProvider() {

        Faker faker = new Faker();
        String email = faker.internet().emailAddress();
        String apiKey = Config.baseApiKey;

        return new Object[][] {
                {email, apiKey},
        };
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void RegistrationTest(String email, String apiKey) {

        JsonPath response = api.registration(email, apiKey);

        assertThat(response.getString("data.registration.token")).isNotNull();
        assertThat(response.getString("data.registration.token")).isNotEmpty();
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void RegistrationWithTmpTokenTest(String email, String apiKey) {

        JsonPath response = api.registration(email, apiKey, currentToken);

        assertThat(response.getString("data.registration.token")).isNotNull();
        assertThat(response.getString("data.registration.token")).isNotEmpty();
        assertThat(response.getString("data.registration.token")).isEqualTo(currentToken);
    }
}