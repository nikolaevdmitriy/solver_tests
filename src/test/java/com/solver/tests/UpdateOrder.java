package com.solver.tests;

import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OrderEntity;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import io.restassured.path.json.JsonPath;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(DataProviderRunner.class)
public class UpdateOrder {

    private static CustomerOrderApi api;
    private static Integer orderId;

    @BeforeClass
    public static void initSpec(){

        api = new CustomerOrderApi();
        OrderEntity order = new OrderEntity();
        orderId = api.create(order.fillFakeData()).getInt("data.createOrder.id");
    }

    @DataProvider
    public static Object[][] dataProvider() {

        OrderEntity order = new OrderEntity();
        order.fillFakeData();

        Integer price = api.price(order).getInt("data.price");

        return new Object[][] {
                {order, price},
        };
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void updateOrderTest(OrderEntity order, Integer price) {

        JsonPath response = api.update(orderId, order);

        assertThat(response.getInt("data.updateOrder.id")).isNotEqualTo(nullValue());
        assertThat(response.getInt("data.updateOrder.id")).isNotEqualTo(empty());
        assertThat(response.getString("data.updateOrder.description")).isEqualTo(order.getDescription());
        assertThat(response.getString("data.updateOrder.categoryId")).isEqualTo(order.getCategoryId().toString());
        assertThat(response.getInt("data.updateOrder.budget")).isEqualTo(price);
    }


    @AfterTest
    public void clearData(){
        api.delete(orderId);
    }
}