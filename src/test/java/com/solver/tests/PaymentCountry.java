package com.solver.tests;

import com.github.javafaker.Faker;
import com.solver.services.api.customer.CustomerOrderApi;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PaymentCountry {

    private static CustomerOrderApi api;

    @BeforeClass
    public static void init(){
        api = new CustomerOrderApi();
    }

    @Test(description = "POST")
    public void paymentCountryTest() {

        JsonPath response = api.paymentCountry();

        assertThat(response.getList("data.dictionarylist.paymentcountry")).isNotEmpty();
    }
}
