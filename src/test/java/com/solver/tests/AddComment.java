package com.solver.tests;

import com.github.javafaker.Faker;
import com.solver.services.api.FacadeApi;
import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OfferEntity;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AddComment {

    private static CustomerOrderApi customerApi;
    private static Integer orderId;
    private static Integer offerId;

    @BeforeClass
    public static void init(){
        customerApi = new CustomerOrderApi();

        OfferEntity offer = FacadeApi.createOffer();
        offerId = offer.getId();
        orderId = offer.getOrderId();
    }

    @DataProvider
    public static Object[][] dataProvider() {

        String text = (new Faker()).chuckNorris().fact();

        return new Object[][] {
                {offerId, text},
        };
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void addCommentTest(Integer offerId, String text) {

        JsonPath response = customerApi.addComment(offerId, text);

        assertThat(response.getString("data.addComment.text")).isEqualTo(text);
    }

    @AfterTest
    public void clearData(){
        customerApi.delete(orderId);
    }
}
