package com.solver.tests;

import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OrderEntity;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class IncreaseDeadline {

    private static CustomerOrderApi api;
    private static Integer orderId;

    @BeforeClass
    public static void initSpec(){

        api = new CustomerOrderApi();
        OrderEntity order = new OrderEntity();
        orderId = api.create(order.fillFakeData()).getInt("data.createOrder.id");
    }

    @Test(description = "POST")
    public void increaseDeadlineTest() {

        JsonPath response = api.increaseDeadline(orderId);

        assertThat(response.getInt("data.increaseDeadline.id")).isEqualTo(orderId);
    }

    @AfterTest
    public void clearData(){
        api.delete(orderId);
    }
}
