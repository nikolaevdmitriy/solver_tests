package com.solver.tests;

import com.solver.services.api.customer.CustomerFileApi;
import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OrderEntity;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import io.restassured.path.json.JsonPath;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(DataProviderRunner.class)
public class UploadFile {

    private static CustomerFileApi customerFileApi;
    private static CustomerOrderApi customerOrderApi;
    private static Integer fileId;

    @BeforeClass
    public static void initSpec(){
        customerFileApi = new CustomerFileApi();
        customerOrderApi = new CustomerOrderApi();
    }

    @DataProvider
    public Object[][] dataProvider() {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("file/autotest.png").getFile());

        return new Object[][] {
                {file},
        };
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void uploadFileTest(File file) {

        JsonPath response = customerFileApi.upload(file);

        assertThat(response.getList("$")).isNotEmpty();
        assertThat(response.getInt("[0]")).isNotNull();

        fileId = response.getInt("[0]");
    }

    @AfterTest
    public void clearData(){
        Integer orderId = customerOrderApi.create((new OrderEntity()).fillFakeData()).getInt("data.createOrder.id");
        String fileHash = customerOrderApi.addFileToOrder(orderId, fileId).getString("data.addFileToOrder.fileHash");

        customerFileApi.delete(fileHash);
        customerOrderApi.delete(orderId);
    }
}