package com.solver.tests;


import com.solver.services.api.customer.CustomerFileApi;
import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OrderEntity;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import io.restassured.path.json.JsonPath;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(DataProviderRunner.class)
public class DeleteFile {

    private static CustomerFileApi customerFileApi;
    private static CustomerOrderApi customerOrderApi;
    private static Integer fileId;
    private static Integer orderId;
    private static String fileHash;

    @BeforeClass
    public void initSpec(){
        customerOrderApi = new CustomerOrderApi();
        orderId = customerOrderApi.create((new OrderEntity()).fillFakeData()).getInt("data.createOrder.id");

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("file/autotest.png").getFile());

        customerFileApi = new CustomerFileApi();
        JsonPath response = customerFileApi.upload(file);
        fileId = response.getInt("[0]");

        fileHash = customerOrderApi.addFileToOrder(orderId, fileId).getString("data.addFileToOrder.fileHash");
    }

    @Test(description = "POST")
    public void deleteFileTest() {
        JsonPath response = customerFileApi.delete(fileHash);
        assertThat(response.getBoolean("data.deleteFile")).isTrue();
    }

    @AfterTest
    public void clearData(){
        customerOrderApi.delete(orderId);
    }
}