package com.solver.tests;

import com.solver.services.api.customer.CustomerFileApi;
import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OrderEntity;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import io.restassured.path.json.JsonPath;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(DataProviderRunner.class)
public class GetFile {

    private static CustomerFileApi customerFileApi;
    private static CustomerOrderApi customerOrderApi;
    private static Integer fileId;
    private static Integer orderId;
    private static String fileHash;

    @BeforeClass
    public void initSpec(){
        customerOrderApi = new CustomerOrderApi();
        orderId = customerOrderApi.create((new OrderEntity()).fillFakeData()).getInt("data.createOrder.id");

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("file/autotest.png").getFile());

        customerFileApi = new CustomerFileApi();
        JsonPath response = customerFileApi.upload(file);
        fileId = response.getInt("[0]");
        fileHash = customerOrderApi.addFileToOrder(orderId, fileId).getString("data.addFileToOrder.fileHash");
    }

    @Test(description = "POST")
    public void getFileTest() {

        customerFileApi.get(fileHash);
    }

    @AfterTest
    public void clearData(){

        customerFileApi.delete(fileHash);
        customerOrderApi.delete(orderId);
    }
}