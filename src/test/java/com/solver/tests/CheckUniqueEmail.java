package com.solver.tests;

import com.github.javafaker.Faker;
import com.solver.scheme.customer.Mutation;
import com.solver.services.Config;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class CheckUniqueEmail {

    private static RequestSpecification spec;

    @BeforeClass
    public static void initSpec(){
        spec = Config.getSpecWithoutToken();
    }

    @Test(description = "POST")
    public void CheckNotUniqueTest()
    {
        String email = "n_dmitriy_91@mail.ru";
        String scheme = Mutation.checkUniqueEmail(email);

        JSONObject requestBody = new JSONObject();
        requestBody.put("query", scheme);

        given()
            .spec(spec)
            .body(requestBody.toString())
            .when()
            .post()
            .then()
            .statusCode(200)
            .content("data.checkUniqueEmail", not(isEmptyOrNullString()))
            .content("data.checkUniqueEmail", is(equalTo(false)))
            .content("data", not(hasKey("errors")));
    }

    @Test(description = "POST")
    public void CheckUniqueTest() {

        Faker faker = new Faker();
        String email = System.nanoTime() + faker.internet().emailAddress();
        String scheme = Mutation.checkUniqueEmail(email);

        JSONObject requestBody = new JSONObject();
        requestBody.put("query", scheme);

        given()
            .spec(spec)
            .body(requestBody.toString())
            .when()
            .post()
            .then()
            .statusCode(200)
            .content("data.checkUniqueEmail", not(isEmptyOrNullString()))
            .content("data.checkUniqueEmail", is(equalTo(true)))
            .content("data", not(hasKey("errors")));
    }
}