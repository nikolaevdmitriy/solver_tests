package com.solver.tests;

import com.github.javafaker.Faker;
import com.solver.services.api.FacadeApi;
import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OfferEntity;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PaymentUrl {

    private static CustomerOrderApi customerApi;
    private static Integer orderId;
    private static Integer offerId;

    @BeforeClass
    public static void init(){
        customerApi = new CustomerOrderApi();

        OfferEntity offer = FacadeApi.createOffer();
        offerId = offer.getId();
        orderId = offer.getOrderId();
    }

    @DataProvider
    public static Object[][] dataProvider() {

        Integer amount = (new Faker()).number().numberBetween(100, 10000);

        return new Object[][] {
                {amount, 20, offerId},
        };
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void paymentUrlTest(Integer amount, Integer paymentMethodId, Integer offerId) {

        JsonPath response = customerApi.paymentUrl(amount, paymentMethodId, offerId);

        assertThat(response.getString("data.paymentUrl")).isNotEmpty();
    }

    @AfterTest
    public void clearData(){
        customerApi.delete(orderId);
    }
}
