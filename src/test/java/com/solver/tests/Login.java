package com.solver.tests;

import com.solver.services.Config;
import com.solver.services.api.customer.CustomerAuthApi;
import com.solver.services.entity.UserEntity;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Login {

    private static CustomerAuthApi api;

    @BeforeClass
    public static void initSpec(){
        api = new CustomerAuthApi();
    }

    @Test(description = "POST")
    public void GetTokenTest() {

        JsonPath response = api.getToken(Config.baseApiKey);

        assertThat(response.getString("data.login.token")).isNotNull();
        assertThat(response.getString("data.login.token")).isNotEmpty();
    }

    @DataProvider
    public static Object[][] dataProvider() {

        UserEntity user = new UserEntity();
        user.setLogin("dmitriynik91@gmail.com");
        user.setPassword("538970");

        return new Object[][] {
                {user},
        };
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void LoginTest(UserEntity user) {

        JsonPath response = api.login(user, Config.baseApiKey);

        assertThat(response.getString("data.login.token")).isNotNull();
        assertThat(response.getString("data.login.token")).isNotEmpty();
    }
}