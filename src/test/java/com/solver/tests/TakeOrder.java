package com.solver.tests;

import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.api.performer.PerformerOrderApi;
import com.solver.services.entity.OfferEntity;
import com.solver.services.entity.OrderEntity;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import io.restassured.path.json.JsonPath;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.nullValue;

@RunWith(DataProviderRunner.class)
public class TakeOrder {

    private static CustomerOrderApi customerApi;
    private static PerformerOrderApi performerApi;
    private static Integer orderId;

    @BeforeClass
    public static void initSpec(){

        customerApi = new CustomerOrderApi();
        performerApi = new PerformerOrderApi();
        OrderEntity order = new OrderEntity();
        orderId = customerApi.create(order.fillFakeData()).getInt("data.createOrder.id");
    }

    @DataProvider
    public static Object[][] dataProvider() {

        OfferEntity offer = new OfferEntity();
        offer.setOrderId(orderId);
        offer.fillFakeMessage();

        return new Object[][] {
                {offer},
        };
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void takeOrderTest(OfferEntity offer) {

        JsonPath response = performerApi.takeOrder(offer);

        assertThat(response.getInt("data.orderCreateOffer.id")).isNotEqualTo(nullValue());
        assertThat(response.getInt("data.orderCreateOffer.origin_bid")).isEqualTo(offer.getBid());
    }

    @AfterTest
    public void clearData(){
        customerApi.delete(orderId);
    }
}