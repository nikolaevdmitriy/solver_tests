package com.solver.tests;

import com.github.javafaker.Faker;
import com.solver.scheme.customer.Mutation;
import com.solver.services.Config;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class AddFirebaseToken {

    private static RequestSpecification spec;

    @BeforeClass
    public static void initSpec(){
        spec = Config.getSpecWithToken();
    }

    @Test(description = "POST")
    public void AddFirebaseTokenTest() {

        Faker faker = new Faker();
        String token = faker.crypto().sha256();
        String scheme = Mutation.addFirebaseToken(token);

        JSONObject requestBody = new JSONObject();
        requestBody.put("query", scheme);

        given()
            .spec(spec)
            .body(requestBody.toString())
            .when()
            .post()
            .then()
            .statusCode(200)
            .content("data.addFirebaseToken", not(isEmptyOrNullString()))
            .content("data.addFirebaseToken", is(equalTo(true)))
            .content("data", not(hasKey("errors")));
    }
}