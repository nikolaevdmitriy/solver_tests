package com.solver.tests;

import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OrderEntity;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import io.restassured.path.json.JsonPath;
import org.junit.runner.RunWith;
import org.testng.annotations.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(DataProviderRunner.class)
public class CreateOrder {

    private static CustomerOrderApi api;
    private static Integer orderId;

    @BeforeClass
    public static void initSpec(){

        api = new CustomerOrderApi();
    }

    @DataProvider
    public static Object[][] dataProvider() {

        OrderEntity order = new OrderEntity();
        return new Object[][] {
                {order.fillFakeData()},
        };
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void createOrderTest(OrderEntity order) {

        JsonPath response = api.create(order);

        assertThat(response.getInt("data.createOrder.id")).isNotNull();
        assertThat(response.getString("data.createOrder.description")).isEqualTo(order.getDescription());
        assertThat(response.getBoolean("data.createOrder.isSolver")).isTrue();

        orderId = response.getInt("data.createOrder.id");
    }


    @AfterTest
    public void clearData(){
        api.delete(orderId);
    }
}