package com.solver.tests;

import com.github.javafaker.Faker;
import com.solver.services.api.customer.CustomerOrderApi;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PaymentMethods {

    private static CustomerOrderApi api;

    @BeforeClass
    public static void init(){
        api = new CustomerOrderApi();
    }

    @DataProvider
    public static Object[][] dataProvider() {

        Integer amount = (new Faker()).number().numberBetween(100, 10000);

        return new Object[][] {
                {amount, 1},
        };
    }

    @Test(description = "POST", dataProvider = "dataProvider")
    public void paymentMethodsTest(Integer amount, Integer countryId) {

        JsonPath response = api.paymentMethods(amount, countryId);

        assertThat(response.getList("data.paymentMethods")).isNotEmpty();
    }
}
