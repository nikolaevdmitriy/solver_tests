package com.solver.tests;

import com.github.javafaker.Faker;
import com.solver.scheme.customer.Mutation;
import com.solver.services.Config;
import com.solver.services.api.customer.CustomerAuthApi;
import com.solver.services.entity.UserEntity;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

public class ChangeUser {

    private static CustomerAuthApi api;
    private static String tmpUserToken;

    @BeforeClass
    public static void initSpec(){
        api = new CustomerAuthApi();

        JsonPath response = api.getToken(Config.baseApiKey);
        tmpUserToken = response.getString("data.login.token");
    }

    @Test(description = "POST")
    public void ChangeUserTest() {

        UserEntity user = new UserEntity();
        user.setLogin("n_dmitriy_91@mail.ru");
        user.setPassword("538970");

        JsonPath response = api.changeUser(user, Config.baseApiKey, tmpUserToken);

        assertThat(response.getString("data.changeUser.token")).isNotNull();
        assertThat(response.getString("data.changeUser.token")).isNotEmpty();
    }
}