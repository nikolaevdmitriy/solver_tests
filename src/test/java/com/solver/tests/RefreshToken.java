package com.solver.tests;

import com.solver.services.Config;
import com.solver.services.api.customer.CustomerAuthApi;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RefreshToken {

    private static CustomerAuthApi api;
    private static String currentToken;

    @BeforeClass
    public static void initSpec(){
        api = new CustomerAuthApi();

        JsonPath response = api.getToken(Config.baseApiKey);
        currentToken = response.getString("data.login.token");
    }

    @Test(description = "POST")
    public void RefreshTokenTest() {

        JsonPath response = api.refreshToken(currentToken);

        assertThat(response.getString("data.refreshToken")).isNotNull();
        assertThat(response.getString("data.refreshToken")).isNotEmpty();
        assertThat(response.getString("data.refreshToken")).isNotEqualTo(currentToken);
    }
}
