package com.solver.tests;

import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OrderEntity;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import io.restassured.path.json.JsonPath;
import org.junit.runner.RunWith;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(DataProviderRunner.class)
public class DeleteOrder {

    private static CustomerOrderApi api;
    private static Integer orderId;

    @BeforeClass
    public static void initSpec(){

        api = new CustomerOrderApi();
        OrderEntity order = new OrderEntity();
        orderId = api.create(order.fillFakeData()).getInt("data.createOrder.id");
    }

    @Test(description = "POST")
    public void deleteOrderTest() {

        JsonPath response = api.delete(orderId);

        assertThat(response.getBoolean("data.deleteOrder")).isEqualTo(true);
    }
}