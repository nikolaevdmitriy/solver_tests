package com.solver.tests;

import com.solver.services.api.customer.CustomerFileApi;
import com.solver.services.api.customer.CustomerOrderApi;
import com.solver.services.entity.OrderEntity;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import io.restassured.path.json.JsonPath;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(DataProviderRunner.class)
public class AddFileToOrder {

    private static CustomerFileApi customerFileApi;
    private static CustomerOrderApi customerOrderApi;
    private static Integer fileId;
    private static String fileHash;
    private static Integer orderId;

    @BeforeClass
    public void initSpec(){
        customerFileApi = new CustomerFileApi();
        customerOrderApi = new CustomerOrderApi();

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("file/autotest.png").getFile());
        JsonPath response = customerFileApi.upload(file);

        fileId = response.getInt("[0]");
        orderId = customerOrderApi.create((new OrderEntity()).fillFakeData()).getInt("data.createOrder.id");
    }

    @Test(description = "POST")
    public void addFileToOrderTest() {

        JsonPath response = customerOrderApi.addFileToOrder(orderId, fileId);

        assertThat(response.getString("data.addFileToOrder.fileHash")).isNotEmpty();
        assertThat(response.getString("data.addFileToOrder.fileHash")).isNotNull();

        fileHash = response.getString("data.addFileToOrder.fileHash");
    }

    @AfterTest
    public void clearData(){
        customerFileApi.delete(fileHash);
        customerOrderApi.delete(orderId);
    }
}