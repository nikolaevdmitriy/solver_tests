package com.solver.tests;

import com.solver.services.api.customer.CustomerOrderApi;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DictionaryList {

    private static CustomerOrderApi api;

    @BeforeClass
    public static void init(){
        api = new CustomerOrderApi();
    }

    @Test(description = "POST")
    public void dictionaryListTest() {

        JsonPath response = api.dictionaryList();

        assertThat(response.getList("data.dictionarylist.maincategories")).isNotEmpty();
        assertThat(response.getList("data.dictionarylist.workcategories")).isNotEmpty();
    }
}
